# project Sync
This is a firebase functions install that takes care of synchronizing Timing App with Activecollab.

After installation
```bash
cd functions
npm i
```

Also install the latest firebase tools
```bash
npm i -g firebase-tools@latest
```