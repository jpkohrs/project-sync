'use strict'
import axios from 'axios'
import { config } from 'firebase-functions'

const { todoist } = config()

let todoistApi = axios.create({
    baseURL: 'https://api.todoist.com/rest/v1',
    headers: {
        'Authorization': `Bearer ${todoist.token}`,
        'Content-type': 'application/json',
        //'X-Request-Id': uuid,
    }
})

export const sync = async () => {
    await todoistApi.get('/')
}