'use strict'
import axios from 'axios'
import * as functions from 'firebase-functions'

const { activecollab } = functions.config()

const activecollabAPI = axios.create({
  baseURL: activecollab.url,
  headers: {
    'Content-Type': 'application/json',
    'X-Angie-AuthApiToken': activecollab.token,
  },
})

interface AcProject {
  id: number
  class: string
  url_path: string
  name: string
  completed_on: number | null
  completed_by_id: number | null
  is_completed: boolean
  members: number[]
  category_id: number
  label_id: number
  is_trashed: boolean
  trashed_on: number | null
  trashed_by_id: number
  created_on: number
  created_by_id: number | null
  created_by_name: string
  created_by_email: string
  updated_on: number
  updated_by_id: number
  body: string
  body_formatted: string
  company_id: number
  leader_id: number
  currency_id: number
  template_id: number
  based_on_type: number | null
  based_on_id: number
  email: string
  is_tracking_enabled: boolean
  is_client_reporting_enabled: boolean
  is_sample: boolean
  budget: number | null
  count_tasks: number
  count_discussions: number
  count_files: number
  count_notes: number
  last_activity_on: number
}

export const get_user_projects = async () => {
  const response = await activecollabAPI.get(
    `/users/${activecollab.userid}/projects`
  )
  if (!response.data || response.status !== 200) {
    return Promise.reject(new Error(`${response.statusText}: Couldn't fetch ac-projects`))
  }
  return Promise.resolve(response.data as AcProject[])
}

export const get_project_tasks = async (projectID: number) => {
  const response = await activecollabAPI.get(`/projects/${projectID}/tasks`)
  //const response2 = await activecollabAPI.get(`/projects/${projectID}/tasks/archive`)

  if (response.status !== 200) {
    return Promise.reject(
      new Error(`${response.statusText}: Couldn't fetch ac-tasks-${projectID}`)
    )
  }
  return Promise.resolve(response.data)
}

export const get_project_tasks_archive = async (projectID: number) => {
  const response = await activecollabAPI.get(`/projects/${projectID}/tasks/archive`)

  if (response.status !== 200) {
    return Promise.reject(
      new Error(`${response.statusText}: Couldn't fetch archive ac-tasks-${projectID}`)
    )
  }
  return Promise.resolve(response.data)
}

export const create_record = async (projectID: number, payload: object) => {
  const response = await activecollabAPI.post(
    `/projects/${projectID}/time-records`,
    payload
  )
  if (response.status !== 200) {
    functions.logger.error(`create_ac_record failed: ${response.statusText}`)
    return Promise.reject(new Error(`create_ac_record failed: ${response.statusText}`))
  }
  return Promise.resolve(response.data)
}

export const update_record = async (url_path: string, payload: object) => {
  const response = await activecollabAPI.put(url_path, payload)

  if (response.status !== 200) {
    functions.logger.error(`update_ac_record failed: ${response.statusText}`)
    return Promise.reject(new Error(`update_ac_record failed: ${response.statusText}`))
  }
  return Promise.resolve(response.data)
}

  // TOOD check if project exists in Timing (OR, how can I work around this?)
// TODO create project at Todoist

// interface TimingProject {
//   self: string
//   title: string
//   title_chain: string[]
//   color: string
//   productivity_score: Number
//   is_archived: boolean
//   parent: {
//     self: string
//   }
//   children?: TimingProject[]
// }

// interface TimingRecord {
//   self: string
//   start_date: string
//   end_date: string
//   duration: number
//   project: TimingProject
//   title: string
//   notes: string | null
//   is_running: boolean
// }

// interface AcTimeRecord {
//   id: number
//   class: string
//   url_path: string
//   is_trashed: boolean
//   trashed_on: number | null
//   trashed_by_id: number
//   billable_status: number
//   value: number
//   record_date: number
//   summary: string
//   user_id: number
//   parent_type: string
//   parent_id: number
//   created_on: number
//   created_by_id: number
//   created_by_name: string
//   created_by_email: string
//   updated_on: number
//   updated_by_id: number
//   job_type_id: number
//   user_name: string
//   user_email: string
//   source: string
// }

