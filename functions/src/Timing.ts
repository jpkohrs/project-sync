'use strict'
import axios from 'axios'
import * as functions from 'firebase-functions'

const { timing } = functions.config()

let timingApi = axios.create({
    baseURL: 'https://web.timingapp.com/api/v1',
    headers: {
      'Authorization': `Bearer ${timing.token}`,
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
  })


// get records from Timing
export const get_records = async () => {
    functions.logger.log(`fetching timing-records`)
    const params = {
        'projects[]': timing.root_project,
        'is_running': 'false',
        'include_project_data': 'false',
        'include_child_projects': 'true',
      }
    const response = await timingApi.get(`/time-entries`, { params })
  
    if (response.status !== 200) {
      return Promise.reject(
        new Error(`${response.statusText}: Couldn't fetch timing-records`)
      )
    }
  
    return Promise.resolve(response.data.data)
  }


export const get_project = async (params:object) => {
    const response = await timingApi.get('/projects', {params})

    return Promise.resolve(response.data.data)
}


export const create_project = async (title:string)=> {
    const response = await timingApi.post('/projects', {
        title,
        parent: timing.root_project,
        color: '#C0FFEE',
        productivity_score: 1,
        is_archived: false,
})
    return Promise.resolve(response.data.data)
}