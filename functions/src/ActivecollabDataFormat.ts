import { pick } from 'lodash'

const keepKeys = {
    'task': [
        'assignee_id',
        'body',
        'class',
        'created_on',
        'due_on',
        'estimate',
        'id',
        'is_billable',
        'is_completed',
        'is_important',
        'is_trashed',
        'job_type_id',
        'name',
        'project_id',
        'start_on',
        'task_number',
        'updated_on',
        'url_path',
      ],
    'project': [
        'id',
        'class',
        'url_path',
        'name',
        'is_completed',
        'is_trashed',
        'created_on',
        'updated_on',
        'body',
        'is_tracking_enabled',
        'is_client_reporting_enabled',
        'count_tasks',
        'count_discussions',
        'count_files',
        'count_notes',
    ]
}

const item_filter = (item:any) => {
  if(item.class === "Task") return pick(item, keepKeys.task)
  if(item.class === "Project") return pick(item, keepKeys.project)
  return item
}

const format_api_data = (data:any[]) => {
    return data.reduce((acc: any, item: any)=> {
      const type: 'task' | 'project' = item.class.toLowerCase()
      const filtered_item = item_filter(item)
      return {
        [`${type}-${item.id}`]: filtered_item, 
        ...acc
      }
    }
    , {})
  }

export default format_api_data