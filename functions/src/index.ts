'use strict'
import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import * as util from 'util'
import * as express from 'express'
import { isEqual } from 'lodash'
import { format, addSeconds, isAfter } from 'date-fns'
import next from 'next'
import {join} from 'path'


admin.initializeApp()
// define common collection references
const db = admin.firestore()
const projectsRef = db.collection('projects')
const tasksRef = db.collection('tasks')
const apiRef = db.collection('api')
// const recordsRef = db.collection('records')
let cutoff_date = new Date(2020, 12, 6)

// Firebase's .env variables. See https://firebase.google.com/docs/functions/config-env
const { activecollab } = functions.config()

if (!activecollab) {
  functions.logger.error(`couldn't load env. variables. Shut down.`)
}

import * as acAPI from './ActivecollabApi'
import acFormat from './ActivecollabDataFormat'
import * as timingAPI from './Timing'

/* CONJOBS */

// CRONJOB for fetching data from ActiveCollab
export const cron_sync_ac = functions.pubsub
  .schedule('every 20 minutes')
  .onRun(async (context) => {
    await fetch_ac_projects()
  })

// CRONJOB for fetching data from TimingApp
export const cron_sync_timing = functions.pubsub
  .schedule('every 20 minutes')
  .onRun(async (context) => {
    await fetch_timing_records()
  })

// CRONJOB for fetching data from TimingApp
export const cron_fix_errors = functions.pubsub
  .schedule('0 17 * * 1-5').timeZone('Europe/Amsterdam')
  .onRun(async (context) => {
    await fix_record_errors()
  })


/* HTTPS CALLS */

// HTTPS CALL to manually init the Sync
export const sync_apis = functions.https.onRequest(async (req, res) => {

  req.url
  // init API calls
  const acProjects = fetch_ac_projects()
  const timingRecords = fetch_timing_records()

  await Promise.all([acProjects, timingRecords])
  res.send(`done: imported 'ac-projects' and 'timing-records'`)
})

export const fix_errors = functions.https.onRequest(async (req, res) => {
  await fix_record_errors()

  res.send(`Triggered records to try again`)
})

const fix_record_errors = async () => {
  // get all records
  const recordsSnap = await db.collection('records').where('error', '==', true).get()
  if (recordsSnap.empty) {
    return
  }

  const writeResults: Promise<FirebaseFirestore.WriteResult>[] = []

  recordsSnap.docs.forEach((doc) => {
    const dbUpdate = doc.ref.set(
      { trigger_onupdate: true },
      { merge: true })

    writeResults.push(dbUpdate)
  })
  functions.logger.info(`trying to fix ${recordsSnap.docs.length} record(s)`)
  await Promise.all(writeResults)
}

export const clear_old_records = functions.https.onRequest(async (req, res) => {
  // get all records
  const recordsSnap = await db.collection('records').get()
  if (recordsSnap.empty) {
    res.send('No Records found')
  }
  let count = 0

  const writeResults: Promise<FirebaseFirestore.WriteResult>[] = []

  recordsSnap.docs.forEach((doc) => {
    const data = doc.data()

    if (data.timing && isAfter(cutoff_date, new Date(data.timing.start_date))) {
      writeResults.push(doc.ref.delete())
      count++
    }
  })

  await Promise.all(writeResults)

  res.send(`Cleared ${count} records`)
})

/*  GET PROJECTS
/   Legacy code to retrieve a fresh list of all projects. 
/   Needed for the offline scripts
*/
export const get_projects = functions.https.onRequest(async (req, res) => {
  // get all projects from database to loop through their IDs to get their tasks
  const allProjects = await projectsRef.get()
  if (allProjects.empty) {
    res.send('No Projects found for tasks')
  }
  const data: any[] = []

  allProjects.forEach((doc) => {
    const { ac, timing } = doc.data()
    if (ac === undefined || timing === undefined) {
      return
    }
    const project = {
      ac_name: ac.name,
      ac_id: ac.id,
      timing_name: timing.title,
      timing_id: timing.self.replace('/projects/', ''),
    }
    data.push(project)
  })
  // res.send(JSON.stringify(data))
  res.json(data)
})

export const get_tasks = functions.https.onRequest(async (req, res) => {
  const allTasks = await tasksRef.get()
  if (allTasks.empty) {
    res.send('No Tasks found')
  }
  const data: any[] = []

  allTasks.forEach((doc) => {
    const { ac } = doc.data()
    if (ac === undefined) {
      return
    }

    const { id, name, task_number, project_id } = ac
    const trimmedTask = { id, name, task_number, project_id }

    data.push(trimmedTask)
  })
  res.json(data)
})


/* Returns a list of tasks that are yours */
export const myTasks = functions.https.onRequest(async (req, res) => {
  const myTasks = await tasksRef
    .where('ac.assignee_id', '==', parseInt(activecollab.userid, 10))
    .orderBy('ac.project_id')
    .orderBy('ac.updated_on', 'desc')
    .get()
  if (myTasks.empty) {
    res.send(`You don't have any tasks`)
    return
  }

  const tasks: any = []
  myTasks.forEach((doc) => {
    const task = doc.data()
    tasks.push(`<tr>
      <td>${task.ac.project_id}</td>
      <td><a href="https://app.activecollab.com/119944${task.ac.url_path}">${task.ac.id}</a></td>
      <td>${task.ac.name}</td>
    </tr>`)
  })
  res.send(
    `<table>${tasks.reduce((a: string, b: string) => a.concat(b))}</table>`
  )
})

/* DATABASE EVENTS */

/*  DETECT API CREATED 
/   This function runs once it finds new API response data document.
/   It iterates through all items and places them right there where they make sense.
/   It also invokes task call if this is the first time the ac-projects data is stored,
*/
export const on_api_created = functions.firestore
  .document('api/{apiID}')
  .onCreate(async (snap, context) => {
    const { apiID } = context.params
    functions.logger.info(`new api entry detected: ${apiID}`)

    const [service, type] = apiID.split('-')

    if (!type) {
      return
    }

    const ref = db.collection(type)

    const data = snap.data()

    const query: Promise<any>[] = []

    // loop through the new data
    for (const key in data) {
      const item = data[key]

      // PROJECTS
      if (apiID === 'ac-projects') {
        query.push(fetch_ac_tasks_for_project(item.id))
      }
      query.push(
        ref
          .doc(key)
          .set({ [service]: item, trigger_onupdate: true }, { merge: true })
      )
    }

    const dbUpdates = await Promise.all(query)
    functions.logger.info(dbUpdates)
  })

/*  DETECT API CHANGES
/   This function triggers whenever any API response data has changed.
/   It tries to find out what changed (created, updated, deleted) 
/   and updates those rows accordingly.
 */
export const on_api_changed = functions.firestore
  .document('api/{apiID}')
  .onUpdate(async (change, context) => {
    const apiID: string = context.params.apiID
    const [service, type] = apiID.split('-')

    if (!type) {
      return
    }
    functions.logger.info(`${apiID} has changed`)

    const { added } = find_api_data_modifications(change)

    // write it in the right collection
    const ref = db.collection(type)


    const query: Promise<any>[] = []

    // loop through the new data
    for (const key in added) {
      functions.logger.info(`${key} updating`)
      const item = added[key]

      query.push(
        ref
          .doc(key)
          .set({ [service]: item, trigger_onupdate: true }, { merge: true })
      )
    }

    await Promise.all(query)
  })

/*  DETECT RECORD CREATED
/   Detects new timing records and tries to creates the ac records
/   To create the ac record, the current task has to be found with
/   the data from the timing record.
*/
export const on_record_created = functions.firestore
  .document('records/{recordID}')
  .onCreate(async (snap, context) => {
    const record = snap.data()!.timing
    if (!record) return

    const { title } = record

    let task: any = {}

    try {
      if (/^#/.test(title)) {
        task = await get_task_by_tasknumber(record)
      } else {
        task = await get_task_by_alias(record)
      }

      const response = await create_ac_record(task, record)

      // safe ac record reference to snapshot
      await snap.ref.set(
        {
          ac: response.single,
          trigger_onupdate: false,
          error: false,
        },
        { merge: true }
      )
      functions.logger.info(`Record written to AC and saved in DB: ${response.single.id}`)
    } catch (err) {
      handleError(err)
      await snap.ref.set(
        {
          error: true,
          errorMsg: err.message, // firebase doesn't like this..
          trigger_onupdate: false,
        },
        { merge: true }
      )
    }
  })

export const on_record_updated = functions.firestore
  .document('records/{recordID}')
  .onUpdate(async (snap, context) => {
    const data = snap.after.data()
    if (!data?.trigger_onupdate) {
      return
    }

    let response: any = {}

    try {
      if (data.ac) {
        // do we need to update?
        response = await update_ac_record(data.ac.url_path, data.timing)
      } else if (data.error) {
        // do we need to try again?
        let task: any = {}
        if (/^#/.test(data.timing.title)) {
          task = await get_task_by_tasknumber(data.timing)
        } else {
          task = await get_task_by_alias(data.timing)
        }
        response = await create_ac_record(task, data.timing)
      }
      await snap.after.ref.set(
        {
          ac: response.single,
          trigger_onupdate: false,
          error: false,
        },
        { merge: true }
      )
      functions.logger.info(`Record written to AC and saved in DB: ${response.single.id}`)
    } catch (err) {
      handleError(err)
      await snap.after.ref.set(
        {
          error: true,
          errorMsg: err.message, // firebase doesn't like this..
          trigger_onupdate: false,
        },
        { merge: true }
      )
    }
  })

// TODO: Rename function to on_project_created, 
// see firebase docs why this can't be done with ease
export const on_project_created = functions.firestore
  .document('projects/{id}')
  .onCreate(async (snap, context) => {
    const projectTitle = snap.data()!.ac.name
    // const projectRef = snap.ref
    functions.logger.info(projectTitle)
    let intervention_needed: boolean = false

    const data = await timingAPI.get_project({ title: projectTitle })

    functions.logger.info(data.length)

    let query: any

    if (data.length === 0) {
      functions.logger.info(`no timing project found, we need to create it...`)
      const timingProject = await timingAPI.create_project(projectTitle)

      query = timingProject
    } else if (data.length > 1) {
      functions.logger.info(
        `We found more than 1 timing project matching this project title...`
      )
      query = data[0]
      intervention_needed = true
      // we got more than one entry, and need to see which description fits best...
    } else {
      functions.logger.info(`We found a matching timing project`)
      query = data[0]
    }

    await snap.ref.set(
      {
        name: projectTitle,
        timing: query,
        intervention_needed,
      },
      { merge: true }
    )

    functions.logger.info(`project ${data.ac.id} has been created , fetching tasks...`)

    await fetch_ac_tasks_for_project(data.ac.id)
    functions.logger.info(`tasks fetched for project ${data.ac.id}`)
  })

export const on_project_changed = functions.firestore
  .document('projects/{id}')
  .onUpdate(async (snap, context) => {
    const data = snap.after.data()
    if (!data?.trigger_onupdate) {
      return
    }
    functions.logger.info(`project ${data.ac.id} has changed, fetching tasks...`)

    await fetch_ac_tasks_for_project(data.ac.id)
    functions.logger.info(`tasks fetched for project ${data.ac.id}`)
  })
// TODO: on_project_update

// TODO: on_task_updated

export const on_task_created = functions.firestore.document('tasks/{id}')
  .onCreate(async (snap, context) => {
    const task = snap.data()!
    functions.logger.info('task created', task)
    const recordsSnap = await db.collection('records').where('ac.id', '==', task.ac.id).where('error', '==', true).get()
    if (recordsSnap.empty) {
      functions.logger.info('no matching records found')
      return
    }
    functions.logger.info(`${recordsSnap.docs.length} matchng records found for ${task.ac.name}`)

    const writeResults: Promise<FirebaseFirestore.WriteResult>[] = []

    recordsSnap.docs.forEach((doc) => {
      writeResults.push(doc.ref.set({ trigger_onupdate: true }, { merge: true }))
    })

    await Promise.all(writeResults)

    return

  })

/* HELPER FUNCTIONS */

// get Projects from AC
const fetch_ac_projects = async () => {
  functions.logger.info(`fetching ac-projects`)

  // fetch data
  const data = await acAPI.get_user_projects()
  functions.logger.log("AC Projects import", data);

  // format to be db ready
  const formattedData = acFormat(data)
  functions.logger.log("AC Projects formatted", formattedData);

  //write to db
  await apiRef.doc('ac-projects').set(formattedData)
  return await Promise.resolve(formattedData)
}

// get Tasks from AC from a specific project
const fetch_ac_tasks_for_project = async (projectID: number) => {

  functions.logger.info(`fetching ac-tasks-${projectID}`)

  // fetch data
  const { tasks, completed_task_ids } = await acAPI.get_project_tasks(projectID)
  // const archived = await acAPI.get_project_tasks_archive(projectID)

  functions.logger.info('Tasks fetched:', tasks)
  // format to be db ready
  const formatted_tasks = acFormat(tasks)
  functions.logger.info('Tasks formatted:', formatted_tasks)

  // write to db
  await apiRef.doc(`ac-tasks-${projectID}`).set(formatted_tasks)
  await completeTasksForProject(completed_task_ids, projectID)
  return await Promise.resolve(formatted_tasks)
}

const completeTasksForProject = async (taskIDs: Number[], projectID: Number) => {
  const snapshot = await tasksRef
    .where("ac.project_id", "==", projectID)
    .where("ac.is_completed", "==", false).get()
  //.where("ac.id", "in", taskIDs)

  if (snapshot.empty) {
    return
  }

  const writeResults: Promise<FirebaseFirestore.WriteResult>[] = []
  snapshot.forEach(doc => {
    const { ac } = doc.data()
    if (taskIDs.includes(ac.id)) {
      functions.logger.info(`task ${ac.id} for project ${projectID} is set to completed`)
      writeResults.push(doc.ref.set({ ac: { is_completed: true } }, { merge: true }))
    }
  })

  await Promise.all(writeResults)
}

// get records from Timing
const fetch_timing_records = async () => {
  functions.logger.info(`fetching timing-records`)

  // fetch data
  const data = await timingAPI.get_records()

  // filter data
  const filtered_records = filter_timing_records(data)

  // format data
  const formatted_records = format_timing_records(filtered_records)
  functions.logger.info('timing records formatted:', formatted_records)


  await apiRef.doc(`timing-records`).set(formatted_records)

  return Promise.resolve(formatted_records)
}

// Filters out entries before 17 April 2020
const filter_timing_records = (records: any) => {
  return records.filter((record: any) =>
    isAfter(cutoff_date, new Date(record.start_date))
  )
}

// Formats the record
const format_timing_records = (data: any[]) => {

  const formattedData = data.reduce((acc, x) => {
    const id = x.self.replace('/time-entries/', 'record-')
    return { [id]: x, ...acc }
  }, {})
  return formattedData
}

const get_task_by_tasknumber = async (record: any) => {
  const { title } = record

  // get tasknumber from title
  const task_number = parseInt(title.split(':')[0].substr(1), 10)

  // get Project from DB
  const projectSnap = await projectsRef
    .where('timing.self', '==', record.project.self)
    .limit(1)
    .get()

  // project doesn't exist
  if (projectSnap.empty) {
    functions.logger.error(`couldn't find ${record.project.self} for '${title}'`)
    return Promise.reject(
      new Error(`couldn't find ${record.project.self} for '${title}'`)
    )
  }

  // project exists
  const project = projectSnap.docs[0].data().ac

  // get task with project id and task number
  const taskSnap = await tasksRef
    .where('ac.task_number', '==', task_number)
    .where('ac.project_id', '==', project.id)
    .limit(1)
    .get()

  // task doesn't exist
  if (taskSnap.empty) {
    functions.logger.error(
      `couldn't find task #${task_number} in ${project!.name} for '${title}'`
    )
    return Promise.reject(
      new Error(
        `couldn't find task #${task_number} in ${project!.name} for '${title}'`
      )
    )
  }

  // task exists
  return Promise.resolve(taskSnap.docs[0].data().ac)
}

const get_task_by_alias = async (record: any) => {
  const taskSnap = await tasksRef
    .where('alias', '==', record.title)
    .limit(1)
    .get()
  if (taskSnap.empty) {
    functions.logger.error(`Couldn't find alias for ${record.title}`)
    return Promise.reject(new Error(`Couldn't find alias for ${record.title}`))
  }

  return Promise.resolve(taskSnap.docs[0].data().ac)
}

const create_ac_record = async (task: any, record: any) => {
  const start_date = new Date(record.start_date)
  const end_date = new Date(record.end_date)

  // seconds of task, rounded to nearest 5 minutes, format to H:mm
  const rounded_duration = Math.round(record.duration / (60 * 5)) * 60 * 5
  const dateObj = addSeconds(new Date(0), rounded_duration)
  const duration = format(dateObj, 'H:mm')

  const summary = `[${format(start_date, 'HH:mm')}-${format(end_date,
    'HH:mm'
  )}] ${record.notes !== null ? record.notes : ''}`

  // create ac record
  const payload: any = {
    user_id: parseFloat(activecollab.userid), // record the time as me
    task_id: task.id,
    job_type_id: task.job_type_id ? task.job_type_id : 1, // go with task setting, ERRORS here
    billable_status: task.is_billable ? 1 : 0, // go with task setting
    value: duration,
    record_date: format(start_date, 'yyyy-MM-dd'), // AC accepts this format, it seems the least ambigious
    summary,
  }

  functions.logger.info(util.inspect(payload))

  const data = await acAPI.create_record(task.project_id, payload)

  return Promise.resolve(data)
  // return Promise.resolve(payload)
}

const update_ac_record = async (url_path: string, record: any) => {
  const start_date = new Date(record.start_date)
  const end_date = new Date(record.end_date)

  // seconds of task, rounded to nearest 5 minutes, format to H:mm
  const rounded_duration = Math.round(record.duration / (60 * 5)) * 60 * 5
  const dateObj = addSeconds(new Date(0), rounded_duration)
  const duration = format(dateObj, 'H:mm')

  const summary = `[${format(start_date, 'HH:mm')}-${format(end_date,
    'HH:mm'
  )}] ${record.notes !== null ? record.notes : ''}`

  // create ac record
  const payload: any = {
    value: duration,
    summary,
  }

  functions.logger.info(payload)

  const data = await acAPI.update_record(url_path, payload)

  return Promise.resolve(data)
}

const find_api_data_modifications = (change: functions.Change<FirebaseFirestore.DocumentSnapshot>) => {
  const oldData = change.before.data()!
  const newData = change.after.data()

  // loop through all data and delete
  for (const key in newData) {
    if (!oldData[key]) continue
    if (isEqual(oldData[key], newData[key])) delete newData[key]
    delete oldData[key]
  }

  return { added: newData, removed: oldData }
}


const handleError = (error: any) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    functions.logger.error(error.response);
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    functions.logger.error(error.request);
  } else {
    // Something happened in setting up the request that triggered an Error
    functions.logger.info('Error', error.message);
  }
  if (error.config) {
    functions.logger.error(error.config);
  }
}

// Separate API to make shortURLS
const app = express()

app.get('/:pid-:tid', async (req, res) => {
  const { pid, tid } = req.params
  res.redirect(301, `https://app.activecollab.com/119944/projects/${pid}/tasks/${tid}`)
})

export const redirect = functions.https.onRequest(app)

const dev = process.env.NODE_ENV !== "production";
const nextApp = next({
  dev,
  // the absolute directory from the package.json file that initialises this module
  // IE: the absolute path from the root of the Cloud Function
  conf: { distDir: join("site",".next") },
});
const handle = nextApp.getRequestHandler();

export const nextServer = functions.https.onRequest((request, response) => {
  // log the page.js file or resource being requested
  console.log("File: " + request.originalUrl);
  return nextApp.prepare().then(() => handle(request, response));
});



// export const set_redirect = functions.https.onRequest(async (req, res) => {

// })
