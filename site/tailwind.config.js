module.exports = {
  purge: ['./pages/**/*.tsx', './components/**/*.tsx', './layouts/**/*.tsx'],
  theme: {
    extend: {},
  },
  variants: {
    margin: ['responsive', 'last'],
  },
  plugins: [],
}
