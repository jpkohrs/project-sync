import Navigation from '../components/NavBar'

const Landing = (props: any) => (
  <>
    <Navigation />
    <main className="p-4 sm:pl-24 sm:pr-8 min-h-screen bg-gray-100">
      <div className="grid col-gap-12 row-gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4">
        {props.children}
      </div>
    </main>
    <style jsx>{`
      main {
        grid-template-columns: repeat(auto-fill, minmax(16rem, 1fr));
      }
    `}</style>
  </>
)

export default Landing
