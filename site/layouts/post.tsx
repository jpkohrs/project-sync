import Navigation from '../components/NavBar'

const Blogpost = (props: any) => (
  <>
    <Navigation />
    <main className="m-4 sm:ml-24 sm:mr-8">{props.children}</main>
  </>
)

export default Blogpost
