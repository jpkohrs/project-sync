import firebase from 'firebase'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyABIGaib81LCb_E_CpvBHCpJf_IQQnO68o',
  authDomain: 'puppet-23bff.firebaseapp.com',
  databaseURL: 'https://puppet-23bff.firebaseio.com',
  projectId: 'puppet-23bff',
  storageBucket: 'puppet-23bff.appspot.com',
  messagingSenderId: '765353002853',
  appId: '1:765353002853:web:de37301f873ad07b4b038c',
}

  firebase.initializeApp(firebaseConfig)


const db = firebase.firestore()

export default db
