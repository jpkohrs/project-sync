import { NextPage } from 'next'
import { formatDistanceStrict } from 'date-fns'
import Post from '@layouts/post'
import db from '@api/firebase-config'
import { Table, Cell } from '@components/Table'

const removeError = async (ref: string) => {
  await db.collection('records').doc(ref).set({ error: false }, { merge: true })
}

const RecordsTable = ({ records }) => (
  <Table columntitles={['Time', 'Title', 'Duration', 'Error', 'Action']}>
    {records.map((record) => (
      <tr key={record.id}>
        <Cell>
          {formatDistanceStrict(
            new Date(record.timing.start_date),
            new Date(),
            {
              addSuffix: true,
            }
          )}
        </Cell>
        <Cell>{record.timing.title}</Cell>
        <Cell>{record.timing.duration}</Cell>
        <Cell>
          <span className="text-xs text-red-500">{record.errorMsg}</span>
        </Cell>
        <Cell>
          <button
            className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
            onClick={() => {
              removeError(record.id)
            }}
          >
            clear
          </button>
        </Cell>
      </tr>
    ))}
  </Table>
)

const Home: NextPage<any> = ({ records }) => (
  <>
    <Post>
      <div className="fullwidth">
        <h1>Time Record Sync errors</h1>
        {records.length > 0 ? (
          <RecordsTable records={records} />
        ) : (
          <div>No faulty records found :D</div>
        )}
      </div>
    </Post>
    <style jsx>{`
      .fullwidth {
        grid-column-start: 2;
        grid-column-end: 5;
      }
    `}</style>
  </>
)

Home.getInitialProps = async () => {
  const snapshot = await db
    .collection('records')
    .where('error', '==', true)
    .orderBy('timing.start_date')
    .get()

  return {
    records: snapshot.docs.map((doc) => {
      return { id: doc.id, ...doc.data() }
    }),
  }
}

export default Home
