import { NextPage } from 'next'
import LandingTemplate from '@layouts/landing'
import Card from '@components/Card'

const Home: NextPage = () => (
  <>
    <LandingTemplate>
      <h1>Reports</h1>
      <Card 
        url="/tasks"
        title="50 Newest tasks"
        desc="50 most recently created tasks, sorted by newest. To keep track of the newest things going on." 
      />
      <Card 
        url="/due"
        title="My due tasks"
        desc="All tasks that have a due date. (Still need to filter completed tasks)" 
      />
      <Card 
        url="/records"
        title="50 newest Time recording"
        desc="What are the latest records taken into DB? Let's find out!" 
      />
      <Card 
        url="/sync"
        title="Time recording errors"
        desc="Time Record errors occured and thus not uploaded to AC. Race conditions are automatically cleaned up daily at 17:00." 
      />
    </LandingTemplate>
  </>
)

export default Home
