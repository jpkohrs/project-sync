import { NextPage } from 'next'
import { useState, useEffect } from 'react'
import { formatDistanceStrict } from 'date-fns'

import Post from '@layouts/post'

import db from '@api/firebase-config'
import { Table, Cell } from '@components/Table'
import CopyButton from '@components/CopyButton'

const TaskTable = ({ tasks, now }) => (
  <Table columntitles={['Time', 'Task', 'ProjectID', 'Actions']}>
    {tasks.map((task) => (
      <tr key={task.ac.id} className="hover:bg-blue-100">
        <Cell>
          {formatDistanceStrict(new Date(task.ac.created_on * 1000), now, {
            addSuffix: true,
          })}
        </Cell>
        <Cell>
          <a href={`https://ac.koh.rs/${task.ac.project_id}-${task.ac.id}`}>
            {`#${task.ac.task_number}: ${task.ac.name}`}
          </a>
        </Cell>
        <Cell>{task.ac.project_id}</Cell>
        <Cell>
          <CopyButton
            code={`#${task.ac.task_number}: ${task.ac.name} https://ac.koh.rs/${task.ac.project_id}-${task.ac.id}`}
          />
        </Cell>
      </tr>
    ))}
  </Table>
)

const Home: NextPage<any> = ({ tasks }) => {
  // useState etc.
  const [myTasks, setTasks] = useState(tasks)
  const [now, setNow] = useState(new Date())
  useEffect(() => {
    // create listener for tasks
    const unsubscribe = db
      .collection('tasks')
      .orderBy('ac.created_on', 'desc')
      .limit(50)
      .onSnapshot((snap) => {
        const newTasks = snap.docs.map((doc) => {
          return { id: doc.id, ...doc.data() }
        })
        setTasks(newTasks)
      })

    // create now updater
    const timer = setInterval(() => setNow(new Date()), 600000)

    // remove listener
    return () => {
      unsubscribe()
      clearInterval(timer)
    }
  }, [])

  return (
    <>
      <Post>
        <div className="fullwidth">
          <h1>Newest 50 tasks</h1>
          {myTasks.length > 0 ? (
            <TaskTable tasks={myTasks} now={now} />
          ) : (
            <div>No tasks found</div>
          )}
        </div>
      </Post>
      <style jsx>{`
        .fullwidth {
          grid-column-start: 2;
          grid-column-end: 5;
        }
      `}</style>
    </>
  )
}

Home.getInitialProps = async () => {
  const snapshot = await db
    .collection('tasks')
    .orderBy('ac.created_on', 'desc')
    .limit(50)
    .get()

  return {
    tasks: snapshot.docs.map((doc) => {
      return { id: doc.id, ...doc.data() }
    }),
  }
}

export default Home
