import { NextPage } from 'next'
import { useState, useEffect } from 'react'
import { formatDistanceStrict } from 'date-fns'

import Post from '@layouts/post'

import db from '@api/firebase-config'
import { Table, Cell } from '@components/Table'
import CopyButton from '@components/CopyButton'

// const TaskTable = ({ tasks, now }) => (
//   <Table columntitles={['Task', 'ProjectID', 'Created', 'Due', 'Actions']}>
//     {tasks.map(({ ac }) => {
//       const { project_id, id, task_number, name, created_on, due_on } = ac
//       const url = `https://ac.koh.rs/${project_id}-${id}`
//       const date_created = new Date(created_on * 1000)
//       const date_due = new Date(due_on * 1000)
//       const has_passed = date_due < now

//       return (
//         <tr key={id} className={'hover:bg-blue-100' + (has_passed ? ` text-red-600` : '')}>
//           <Cell>
//             <a
//               className={has_passed ? ` text-red-600` : ''}
//               href={url}
//             >{`#${task_number}: ${name}`}</a>
//           </Cell>
//           <Cell>{project_id}</Cell>
//           <Cell>
//             {formatDistanceStrict(date_created, now, {
//               addSuffix: true,
//             })}
//           </Cell>
//           <Cell>
//             {formatDistanceStrict(date_due, now, {
//               addSuffix: true,
//             })}
//           </Cell>
//           <Cell>
//             <CopyButton code={`#${task_number}: ${name} ${url}`} />
//           </Cell>
//         </tr>
//       )
//     })}
//   </Table>
// )

const TaskList = ({tasks, now}) => (
  <ul>
  {tasks.map(({ ac }) => {
      const { project_id, id, task_number, name, due_on } = ac
      const url = `https://ac.koh.rs/${project_id}-${id}`
      const date_due = new Date(due_on * 1000)
      const date_due_string = due_on <= 0 ? 'no due date' : formatDistanceStrict(date_due, now, {addSuffix: true})
      const has_passed = date_due < now

      return (
      <li key={id} className="bg-white p-2 rounded flex max-w-4xl hover:bg-blue-100">
        <div className="flex-auto">
          <h3 className="text-lg"><a href={url}>{`#${task_number}: ${name}`}</a></h3>
          <p className="text-xs">
            <span>{project_id}</span> | <span className={has_passed ? ` text-red-600` : ''}>{date_due_string}</span>
          </p>
        </div>
        <div><CopyButton code={`#${task_number}: ${name} ${url}`} /></div>
      </li>
      )
   })
  }
  </ul>
)

const Home: NextPage<any> = ({ tasks }) => {
  // useState etc.
  const [myTasks, setTasks] = useState(tasks)
  const [now, setNow] = useState(new Date())

  useEffect(() => {
    // create listener for tasks
    const unsubscribe = db
      .collection('tasks')
      .where('ac.assignee_id', '==', 27)
      .where('ac.is_completed', '==', false)
      //.where('ac.due_on', '>=', 0)
      //.orderBy('ac.due_on', 'asc')
      //.limit(50)
      .onSnapshot((snap) => {
        const newTasks = snap.docs.map((doc) => {
          return doc.data()
        })
        setTasks(newTasks)
      })

    // create now updater
    const timer = setInterval(() => setNow(new Date()), 600000)

    // remove listener
    return () => {
      unsubscribe()
      clearInterval(timer)
    }
  }, [])

  const due_tasks = myTasks.filter((task)=>task.ac.due_on >= 1).sort((a, b)=> a.ac.due_on - b.ac.due_on)
  const unset_tasks = myTasks.filter((task)=>task.ac.due_on <= 0).sort((a, b)=> a.ac.project_id - b.ac.project_id)


  return (
    <>
      <Post>
        <h1 className="text-xl font-bold pt-8 pb-8">My due tasks</h1>
        {due_tasks.length > 0 && (
          <>
            <h2 className="text-xl pt-4">due</h2>
            <TaskList tasks={due_tasks} now={now} />
          </>
        )}
        {unset_tasks.length > 0 && (
          <>
            <h2 className="text-xl pt-4">unset</h2>
            <TaskList tasks={unset_tasks} now={now} />
          </>
        )}
      </Post>
    </>
  )
}

Home.getInitialProps = async () => {
  const snap = await db
    .collection('tasks')
    .where('ac.assignee_id', '==', 27)
    .where('ac.is_completed', '==', false)
    //.where('ac.due_on', '>=', 0)
    //.orderBy('ac.due_on', 'asc')
    //.limit(50)
    .get()

  const newTasks = snap.docs.map((doc) => {
    return { id: doc.id, ac: { due_on: null }, ...doc.data() }
  })
  const filteredTasks = newTasks.filter((task) => task.ac.due_on !== null)
  return { tasks: filteredTasks }
}

export default Home
