import { NextPage } from 'next'
import { useState, useEffect } from 'react'
import { formatDistanceStrict } from 'date-fns'

import Post from '@layouts/post'

import db from '@api/firebase-config'

const RecordList = ({records}) => (
  <ul>
  {records.map(({ timing, ac }) => {
      const { duration, end_date, title, notes } = timing
      const enddate = new Date(end_date)

      return (
      <li key={ac.id} className="bg-white p-2 rounded flex max-w-4xl hover:bg-blue-100">
        <div className="flex-auto">
          <h3 className="text-lg">{title}</h3>
          <p className="text-xs">
            <span>{enddate.toDateString()}</span> | <span>{duration}</span> | <span>{notes}</span>
          </p>
        </div>
      </li>
      )
   })
  }
  </ul>
)

const Home: NextPage<any> = ({ records }) => {
  // useState etc.
  const [myrecords, setrecords] = useState(records)
  useEffect(() => {
    // create listener for records
    const unsubscribe = db
      .collection('records')
      .orderBy('timing.start_date', 'desc')
      .limit(50)
      .onSnapshot((snap) => {
        const newrecords = snap.docs.map((doc) => {
          return { id: doc.id, ...doc.data() }
        })
        setrecords(newrecords)
      })

    // remove listener
    return () => {
      unsubscribe()
    }
  }, [])

  return (
    <>
      <Post>
        <div className="fullwidth">
          <h1>Newest 50 records</h1>
          {myrecords.length > 0 ? (
            <RecordList records={myrecords}  />
          ) : (
            <div>No records found</div>
          )}
        </div>
      </Post>
      <style jsx>{`
        .fullwidth {
          grid-column-start: 2;
          grid-column-end: 5;
        }
      `}</style>
    </>
  )
}

Home.getInitialProps = async () => {
  const snapshot = await db
    .collection('records')
    .orderBy('timing.start_date', 'desc')
    .limit(50)
    .get()

  return {
    records: snapshot.docs.map((doc) => {
      return { id: doc.id, ...doc.data() }
    }),
  }
}

export default Home
