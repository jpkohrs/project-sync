import Link from 'next/link'
import { Url } from 'url'

interface Props {
  icon: string
  title: string
  to: Url | string
}

const LinkItem = (props: Props) => (
  <li className="w-full cursor-pointer last:mt-auto hover:bg-gray-900">
    <Link href={props.to}>
      <div className="item m-4 sm:m-0 sm:w-16 sm:h-16 flex flex-col sm:flex-row text-white items-center transition-all duration-500 ease-in-out">
        <img
          className="w-8 h-8 m-1 sm:w-6 sm:h-6 sm:m-5"
          alt={props.title}
          src={props.icon}
        />
        <span className="label sm:hidden sm:mr-8 text-white">
          {props.title}
        </span>
      </div>
    </Link>
  </li>
)

const Navigation = (props: any) => (
  <nav className="z-10 navigation bg-gray-800 fixed left-0 right-0 bottom-0 sm:top-0 sm:right-auto sm:bottom-auto mb-0">
    <ul className="p-0 m-0 h-auto sm:h-screen flex justify-around sm:flex-col">
      <LinkItem icon="/assets/list.svg" title="Tasks" to="/tasks" />
      <LinkItem icon="/assets/stopwatch.svg" title="Records" to="/sync" />
      <LinkItem icon="/assets/moon.svg" title="Dark&nbsp;Mode" to="/darkmode" />
    </ul>
  </nav>
)

export default Navigation
