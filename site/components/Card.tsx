import Link from 'next/link'

const Card = ({url,title,desc}) => (
<div className="bg-white rounded overflow-hidden shadow-lg cursor-pointer hover:shadow-xl hover:scale-105 transition transform duration-300 ease-in-out">
<Link href={url} passHref>
  <div className="px-6 py-4">
    <h2 className="text-blue-800 text-lg mb-2 font-bold">
      <a>{title}</a>
    </h2>
    <p className="text-gray-700 text-base">
      {desc}
    </p>
  </div>
</Link>
</div>
)

export default Card