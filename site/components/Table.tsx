const tableStyle = 'border w-auto px-4 py-2'
export const Table = ({ columntitles, children }) => (
  <table className="w-full lg:max-w-screen-xl">
    <thead>
      <tr>
        {columntitles.map((title) => (
          <th key={title} className={tableStyle}>
            {title}
          </th>
        ))}
      </tr>
    </thead>
    <tbody>{children}</tbody>
  </table>
)

export const Cell = ({ children }) => <td className={tableStyle}>{children}</td>
