
import useCopyToClipboard from "@api/CopyToClipboard";

function CopyButton({ code }) {
  const {isCopied, handleCopy} = useCopyToClipboard(3000);

  return (
    <button onClick={() =>{ if(handleCopy) handleCopy(code)}} className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
      {isCopied ? <img src="/assets/done.svg" alt="done" /> : <img src="/assets/content_copy.svg" alt="copy to clipboard" />}
    </button>
  );
}

export default CopyButton